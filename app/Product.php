<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['sku', 'name', 'description', 'price', 'stock'];

    public function getPriceAttribute($value)
    {
        return number_format($value, 2);
    }

    /**
     * Check stock
     *
     * @param $quantity
     *
     * @return bool
     */
    public function haveInStock($quantity)
    {
        return $this->stock >= $quantity;
    }
}
