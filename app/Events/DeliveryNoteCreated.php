<?php

namespace App\Events;

use App\DeliveryNote;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeliveryNoteCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var DeliveryNote
     */
    public $deliveryNote;

    /**
     * Create a new event instance.
     *
     * @param  DeliveryNote  $deliveryNote
     */
    public function __construct(DeliveryNote $deliveryNote)
    {
        $this->deliveryNote = $deliveryNote;
    }

}
