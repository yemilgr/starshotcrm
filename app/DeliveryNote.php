<?php

namespace App;

use App\Events\DeliveryNoteCreated;
use Illuminate\Database\Eloquent\Model;

class DeliveryNote extends Model
{
    protected $fillable = ['client_id', 'product_id', 'quantity'];

    protected $dispatchesEvents = [
        'saved' => DeliveryNoteCreated::class
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function gettotalPriceAttribute()
    {
        return number_format(($this->product->price * $this->quantity), 2);
    }
}
