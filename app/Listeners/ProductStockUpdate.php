<?php

namespace App\Listeners;

use App\Events\DeliveryNoteCreated;
use App\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProductStockUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeliveryNoteCreated  $event
     * @return void
     */
    public function handle($event)
    {
        $deliveryNote = $event->deliveryNote;
        Product::where('id', $deliveryNote->product->id)
            ->decrement('stock', $deliveryNote->quantity);
    }
}
