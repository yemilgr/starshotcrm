<?php

namespace App\Http\Controllers;

use App\Client;
use App\DeliveryNote;
use App\Http\Requests\StoreDeliveryNoteRequest;
use App\Product;

class DeliveryNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('delivery_note.index', [
            'notes' => DeliveryNote::with(['client', 'product'])->get(),
        ]);
    }

    public function create()
    {
        return view('delivery_note.create', [
            'clients'  => Client::pluck('name', 'id'),
            'products' => Product::pluck('name', 'id'),
        ]);
    }

    public function store(StoreDeliveryNoteRequest $request)
    {
        $input = $request->validated();
        $product = Product::find($input['product_id']);

        if( ! $product->haveInStock($input['quantity'])){
            return redirect()->back()
                ->with('error', 'Product out of stock')
                ->withInput();
        }

        DeliveryNote::create($input);
        return redirect()->route('deliveryNotes.index')->with('success', 'DeliveryNote created');
    }

    public function destroy(DeliveryNote $deliveryNote)
    {
        $deliveryNote->delete();

        return redirect()->route('deliveryNotes.index')->with('success', 'DeliveryNote deleted');
    }
}
