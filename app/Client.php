<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'email'];

    public function deliveryNotes()
    {
        return $this->hasMany(DeliveryNote::class);
    }
}
