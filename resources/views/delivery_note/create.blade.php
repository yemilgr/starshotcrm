@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Add new DeliveryNote</h5>
            <hr>
            {{ Form::open(['route' => 'deliveryNotes.store']) }}
            <div class="form-group row">
                <label for="client" class="col-sm-2 col-form-label ">Client</label>
                <div class="col-sm-8">
                    {{ Form::select('client_id', $clients, null, ['class' => 'form-control', 'placeholder' => 'Pick a client...', 'required' => 'required']) }}
                </div>
            </div>

            <div class="form-group row">
                <label for="product" class="col-sm-2 col-form-label ">Product</label>
                <div class="col-sm-8">
                    {{ Form::select('product_id', $products, null, ['class' => 'form-control', 'placeholder' => 'Pick a product...', 'required' => 'required']) }}
                </div>
            </div>

            <div class="form-group row">
                <label for="quantity" class="col-sm-2 col-form-label ">Quantity</label>
                <div class="col-sm-8">
                    {{ Form::number('quantity', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-sm-8 offset-sm-2">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{ route('deliveryNotes.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
