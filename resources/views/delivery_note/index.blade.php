@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-8">
                    <h5 class="card-title">DeliveryNote List</h5>
                </div>
                <div class="col-4 text-right">
                    <a href="{{ route('deliveryNotes.create') }}" class="btn btn-sm btn-primary">Add Delivery Note</a>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Client</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
                    <th>Created</th>
                    <th class="text-right">Actions</th>
                </tr>
                </thead>

                <tbody>
                @foreach($notes as $note)
                    <tr>
                        <td>{{ '#' . $note->id }}</td>
                        <td><a href="{{ route('clients.edit', $note->client->id) }}">{{ $note->client->name }}</a></td>
                        <td><a href="{{ route('products.edit', $note->product->id) }}">{{ $note->product->name }}</a></td>
                        <td>{{ $note->quantity }}</td>
                        <td>{{ $note->totalPrice }}</td>
                        <td>{{ $note->created_at }}</td>
                        <td class="text-right">
                            <form action="{{ route('deliveryNotes.destroy', $note->id) }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
