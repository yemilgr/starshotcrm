@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Add new product</h5>

            <hr>

            {{ Form::open(['route' => 'products.store']) }}

            <div class="form-group row">
                <label for="sku" class="col-sm-2 col-form-label ">SKU</label>
                <div class="col-sm-8">
                    <input type="text" name="sku" class="form-control @error('sku')is-invalid @enderror" id="sku">
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label ">Name</label>
                <div class="col-sm-8">
                    <input type="text" name="name" class="form-control @error('name')is-invalid @enderror" id="name">
                </div>
            </div>

            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label ">Description</label>
                <div class="col-sm-8">
                    <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label ">Price</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>
                        <input type="number" name="price" step="0.25" class="form-control @error('price')is-invalid @enderror" id="price" placeholder="0.00">
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="stock" class="col-sm-2 col-form-label ">In Stock</label>
                <div class="col-sm-4">
                    <input type="number" step="1" name="stock" class="form-control @error('stock')is-invalid @enderror" id="stock">
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <div class="col-sm-8 offset-sm-2">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{ route('products.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@endsection
