@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Edit product</h5>
            <hr>
            {{ Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'put']) }}
            <div class="form-group row">
                <label for="sku" class="col-sm-2 col-form-label ">SKU</label>
                <div class="col-sm-8">
                    {{ Form::text('sku', null, ['class' => 'form-control', 'id' => 'sku', 'disabled' => 'disabled']) }}
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label ">Name</label>
                <div class="col-sm-8">
                    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
                </div>
            </div>

            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label ">Description</label>
                <div class="col-sm-8">
                    {{ Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'cols' => '30', 'rows' => '10']) }}
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label ">Price</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>
                        {{ Form::number('price', null, ['class' => 'form-control', 'step' => '0.25']) }}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="stock" class="col-sm-2 col-form-label ">In Stock</label>
                <div class="col-sm-4">
                    {{ Form::number('stock', null, ['class' => 'form-control', 'step' => '1']) }}
                </div>
            </div>

            <hr>
            <div class="form-group row">
                <div class="col-sm-8 offset-sm-2">
                    <button type="submit" class="btn btn-success">Update</button>
                    <a href="{{ route('products.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@endsection
