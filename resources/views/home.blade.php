@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card mb-3" style="max-width: 20rem;">
                <div class="card-body">
                    <h4 class="card-title"><span class="mr-4" style="font-size: 3rem">{{ $clients }}</span> Clients</h4>
                </div>
                <div class="card-footer">
                    <a href="{{ route('clients.index') }}" class="btn btn-sm btn-secondary">Go to list</a>
                    <a href="{{ route('clients.create') }}" class="btn btn-sm btn-primary">Create New</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-3" style="max-width: 20rem;">
                <div class="card-body">
                    <h4 class="card-title"><span class="mr-4" style="font-size: 3rem">{{ $products }}</span> Products</h4>
                </div>
                <div class="card-footer">
                    <a href="{{ route('products.index') }}" class="btn btn-sm btn-secondary">Go to list</a>
                    <a href="{{ route('products.create') }}" class="btn btn-sm btn-primary">Create New</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-3" style="max-width: 20rem;">
                <div class="card-body">
                    <h4 class="card-title"><span class="mr-4" style="font-size: 3rem">{{ $notes }}</span> DeliveryNotes</h4>
                </div>
                <div class="card-footer">
                    <a href="{{ route('deliveryNotes.index') }}" class="btn btn-sm btn-secondary">Go to list</a>
                    <a href="{{ route('deliveryNotes.create') }}" class="btn btn-sm btn-primary">Create New</a>
                </div>
            </div>
        </div>
    </div>
@endsection
