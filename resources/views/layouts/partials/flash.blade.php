@if ($message = session()->get('success'))
    <div class="flash alert alert-success alert-block alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><i class="fas fa-check-circle"></i> Good job!</strong> {{ $message }}
    </div>
@endif

@if ($message = session()->get('error'))
    <div class="flash alert alert-danger alert-block alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><i class="fas fa-exclamation-circle"></i> Whoops!</strong> {{ $message }}
    </div>
@endif

@if ($message = session()->get('warning'))
    <div class="flash alert alert-warning alert-block alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><i class="fas fa-exclamation-circle"></i> Warning!</strong> {{ $message }}
    </div>
@endif

@if ($message = session()->get('info'))
    <div class="flash alert alert-info alert-block alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><i class="fas fa-info-circle"></i> Information!</strong> {{ $message }}
    </div>
@endif

@if ($message = session()->get('important'))
    <div class="flash alert alert-info alert-block alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><i class="fas fa-info-circle"></i> Important!</strong> {{ $message }}
    </div>
@endif

