@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Add new client</h5>
            <hr>
            {{ Form::open(['route' => 'clients.store']) }}
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label ">Name</label>
                <div class="col-sm-8">
                    <input type="text" name="name" class="form-control @error('name')is-invalid @enderror" id="name">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label ">Email</label>
                <div class="col-sm-8">
                    <input type="email" name="email" class="form-control @error('email')is-invalid @enderror" id="email">
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-sm-8 offset-sm-2">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{ route('clients.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@endsection
