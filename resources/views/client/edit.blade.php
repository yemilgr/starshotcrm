@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Edit client</h5>
            <hr>
            {{ Form::model($client, ['route' => ['clients.update', $client->id], 'method' => 'put']) }}
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label ">Name</label>
                <div class="col-sm-8">
                    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label ">Email</label>
                <div class="col-sm-8">
                    {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'required' => 'required']) }}
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-sm-8 offset-sm-2">
                    <button type="submit" class="btn btn-success">Update</button>
                    <a href="{{ route('clients.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@endsection
