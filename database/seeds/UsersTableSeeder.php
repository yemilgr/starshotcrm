<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin CRM',
            'email' => 'admin@starshotcrm.test',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
    }
}
