<?php

use App\DeliveryNote;
use Illuminate\Database\Seeder;

class DeliveryNotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DeliveryNote::class, 5)->create();
    }
}
