<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\DeliveryNote;
use App\Product;
use Faker\Generator as Faker;


$factory->define(DeliveryNote::class, function (Faker $faker) {
    $product = Product::all()->random();
    $client = Client::all()->random();

    return [
        'client_id'  => $client->id,
        'product_id' => $product->id,
        'quantity'   => $faker->numberBetween(1, $product->stock),
    ];
});
