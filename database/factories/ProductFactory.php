<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'sku'         => $faker->numerify('SKU-#####'),
        'name'        => $faker->word,
        'description' => $faker->paragraph,
        'price'       => $faker->randomNumber(2),
        'stock'       => $faker->randomNumber(1),
    ];
});
