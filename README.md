## StarshotCRM

CRM test project for Starshot 

## Requirements

- https://laravel.com/docs/6.x#server-requirements
- php sqlite extension

## Installation Instructions

- git clone git@gitlab.com:yemilgr/starshotcrm.git
- cd starshotcrm
- cp .env.example .env
- touch database/database.sqlite
- composer install --optimize-autoloader
- php artisan migrate:fresh --seed
- php artisan serve

### Login Credentials

login: admin@starshotcrm.test

passw: 123456
